package com.springboot.basic;

import com.springboot.basic.configure.AppConfig;
import com.springboot.basic.entity.Customer;
import com.springboot.basic.entity.Item;
import com.springboot.basic.entity.Order;
import com.springboot.basic.service.DebitCardPaymentService;
import com.springboot.basic.service.PaymentService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;

public class ShoppingApplication {
    public static void main(String[] args) throws InterruptedException {
        Customer customer = new Customer();
        customer.setName("Shirish");
        customer.setAddress("Pune - 411015");
        Item item = new Item();
        item.setName("Redmi Note 10 Pro Mobile");
        item.setPricePerIem(14999.00);
        Item item1 = new Item();
        item1.setName("Pen Drive 32Gb");
        item1.setPricePerIem(349.00);

        Order order = new Order();
        order.setCustomer(customer);
        order.setId("1234");
        order.setItemsList(Arrays.asList(item, item1));

        //PaymentService paymentService = new PaymentService();
        //ApplicationContext context = new ClassPathXmlApplicationContext("app-config.xml");
        //ApplicationContext context = new AnnotationConfigApplicationContext("com.springboot.basic.service");
        ApplicationContext context = new AnnotationConfigApplicationContext(com.springboot.basic.configure.AppConfig.class);
        PaymentService paymentService = context.getBean(PaymentService.class);
        paymentService.makePayment(order);

        ((AbstractApplicationContext)context).close();

        Thread.currentThread().sleep(5000);

    }
}
