package com.springboot.basic.service;

interface CardPaymentService {
    double debitAmount(double amount);
}
