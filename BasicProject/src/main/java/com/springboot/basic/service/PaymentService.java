package com.springboot.basic.service;

import com.springboot.basic.entity.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class PaymentService implements InitializingBean, DisposableBean {
//public class PaymentService{
    @Value("${name}")
    private String name;
    private static Logger logger = LoggerFactory.getLogger(PaymentService.class);
    private CardPaymentService cardPaymentService;

    // constructor dependency Injection
    @Autowired
    public PaymentService(@Qualifier("debitCardPaymentService") CardPaymentService cardPaymentService) {
        this.cardPaymentService = cardPaymentService;
    }

    public double makePayment(Order order) throws InterruptedException{
        // logger.info(name);
        logger.info(name+" process started for "+order);
        Thread.currentThread().sleep(5000);
        return cardPaymentService.debitAmount(order.getAmount());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("Payment Service bean created at "+new Date());
    }

    // this method work only when explicitly close context
    @Override
    public void destroy() throws Exception {
        logger.info("Payment Service bean destroyed at "+new Date());
    }
}

// precedence : @Qualifier, @Primary
// @PostConstruct
// @PreDestroy