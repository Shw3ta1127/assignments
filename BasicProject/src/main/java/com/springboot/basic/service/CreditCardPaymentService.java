package com.springboot.basic.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class CreditCardPaymentService implements CardPaymentService{
    private static Logger logger = LoggerFactory.getLogger(PaymentService.class);

    @Value("${application.enable.creditcard.payment}")
    private boolean enableCreditcardPayment;

    public double debitAmount(double amount) {
        logger.info("Using Credit card service to make payment of " + amount);
        return amount;
    }
}
