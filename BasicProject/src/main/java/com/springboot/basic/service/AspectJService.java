package com.springboot.basic.service;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Date;

@Aspect
@Component
public class AspectJService {
    private Logger logger = LoggerFactory.getLogger(AspectJService.class);

    @Before("execution(* com.springboot.basic.service.*.*(..))")
    // @Before/@After/@Around("pointcut expression")
    // pointcut expression : execution(return-type package.class.method.(arg))
    public void AdviceStarted(){
        logger.info("The process started at " + new Date());
    }

    @Before("execution(* com.springboot.basic.service.*.*(..))")
    // @Before/@After/@Around("pointcut expression")
    // pointcut expression : execution(return-type package.class.method.(arg))
    public void AdviceEnded(){
        logger.info("The process started at " + new Date());
    }
}
