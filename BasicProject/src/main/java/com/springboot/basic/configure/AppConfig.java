package com.springboot.basic.configure;

import com.springboot.basic.service.PaymentService;
import com.springboot.basic.service.CreditCardPaymentService;
import com.springboot.basic.service.DebitCardPaymentService;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan("com.springboot.basic.service")
@PropertySource("application.properties")
@EnableAspectJAutoProxy
public class AppConfig {
    /*

    @Bean(name="paymentService")
    public PaymentService getPaymentService(){
        return new PaymentService();
    }

     */
}
