import customers.*;
import exceptions.*;
import managers.CarFuelManager;
import managers.CarServiceManager;
import managers.CarServiceStatus;
import managers.CarWashManager;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner scanner = new Scanner(System.in);

        Car car = new Car();
        Customer customer = new Customer();
        CarServiceStatus status = new CarServiceStatus();
        CarServiceManager carServiceManager = new CarServiceManager();
        CarFuelManager carFuelManager = new CarFuelManager();
        CarWashManager carWashManager = new CarWashManager();

        carWashManager.setWaterAvailable(true);
        carFuelManager.setAvailableFuel(100);

        boolean isValidCustomer;
        boolean isWashingDone;
        boolean isFuelingDone;

        while (true){
            System.out.print("Next Customer enter yes/no : ");
            if(scanner.next().equalsIgnoreCase("no"))
                break;

            userEntries(scanner, car, customer);

            isWashingDone = false;
            isFuelingDone = false;
            isValidCustomer = false;

            try {
                isValidCustomer = carServiceManager.validateCustomer(customer);
            }
            catch (CapacityLessThanAvailableFuelException | NegativeValueException | CapacityZeroException e){
                System.out.println(e.getMessage());
            }

            if(! isValidCustomer){
                continue;
            }

            status.setFuelStatus(false);
            status.setWashStatus(false);
            status.setCar(car);

            System.out.println("\nCar Full Servicing Started...");
            try{
                isWashingDone = carWashManager.carWashing(car);
                isFuelingDone = carFuelManager.carFueling(car);
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
            System.out.println("Car Full Servicing Completed");
            carServiceManager.generateReport(status);
        }

    }

    private static void userEntries(Scanner scanner, Car car, Customer customer) {
        System.out.print("Enter Customer Name : ");
        customer.setName(scanner.next());

        System.out.print("Enter Customer Phone : ");
        customer.setPhone(scanner.next());

        System.out.print("Enter Car Type : ");
        car.setType(scanner.next());

        System.out.print("Enter Car Available Fuel : ");
        car.setAvailableFuel(scanner.nextInt());

        System.out.print("Enter Car Fuel Capacity : ");
        car.setCapacity(scanner.nextInt());

        customer.setCar(car);
    }
}
