package managers;

import customers.Car;
import exceptions.WaterNotAvailable;

public class CarWashManager extends Manager{
    private boolean isWaterAvailable = false;

    public boolean isWaterAvailable() {
        return isWaterAvailable;
    }

    public void setWaterAvailable(boolean waterAvailable) {
        isWaterAvailable = waterAvailable;
    }

    public boolean carWashing(Car car) throws WaterNotAvailable {
        if (isWaterAvailable){
            return true;
        }
        throw new WaterNotAvailable();
    }
}
