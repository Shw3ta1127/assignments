package managers;

import customers.Car;
import exceptions.FuelNotAvailable;
import exceptions.TankFullException;

public class CarFuelManager extends Manager{
    private int availableFuel;

    public int getAvailableFuel() {
        return availableFuel;
    }

    public void setAvailableFuel(int availableFuel) {
        this.availableFuel = availableFuel;
    }

    public boolean carFueling(Car car){
        if(car.getCapacity() == car.getAvailableFuel())
            throw new TankFullException();
        if(this.availableFuel == 0)
            throw new FuelNotAvailable();
        if(this.availableFuel < (car.getCapacity() - car.getAvailableFuel()))
            throw new FuelNotAvailable();

        this.availableFuel -= car.getCapacity() - car.getAvailableFuel();
        car.setAvailableFuel(car.getCapacity());
        return true;
    }

}
