package managers;

import javax.persistence.*;

import customers.Car;

@Entity
@Table(name = "CARSERVICESTATUS")
public class CarServiceStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private boolean fuelStatus;
    private boolean washStatus;

    @OneToOne(cascade = CascadeType.ALL)
    private Car car;

    public boolean isFuelStatus() {
        return fuelStatus;
    }

    public void setFuelStatus(boolean fuelStatus) {
        this.fuelStatus = fuelStatus;
    }

    public boolean isWashStatus() {
        return washStatus;
    }

    public void setWashStatus(boolean washStatus) {
        this.washStatus = washStatus;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public CarServiceStatus() {
        super();
    }
}
