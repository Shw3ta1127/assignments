package managers;

import customers.Car;
import customers.Customer;
import exceptions.CapacityLessThanAvailableFuelException;
import exceptions.CapacityZeroException;
import exceptions.NegativeValueException;

public class CarServiceManager extends Manager{
    // private List<CarServiceStatus> statuses;

    public boolean validateCustomer(Customer customer){
        if(customer.getCar().getCapacity() == 0)
            throw new CapacityZeroException();
        if(customer.getCar().getAvailableFuel() < 0 || customer.getCar().getCapacity() < 0)
            throw new NegativeValueException();
        if(customer.getCar().getCapacity() < customer.getCar().getAvailableFuel())
            throw new CapacityLessThanAvailableFuelException();
        return true;
    }
    public void generateReport(CarServiceStatus status) {
        System.out.println("\nReport");
        System.out.println("Car Washing " + status.isWashStatus());
        System.out.println("Car Fueling " + status.isFuelStatus());
        System.out.println("\n");
    }
}
