package repo;

import customers.Customer;
import org.hibernate.Session;

public class CustomerDao {

    public void saveCustomer(Customer customer){
        Session session = DBManager.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(customer);
        session.getTransaction().commit();
    }

    public void updateCustomer(Customer customer){
        Session session = DBManager.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.update(customer);
        session.getTransaction().commit();
    }

    public void deleteCustomer(int id){
        Session session = DBManager.getSessionFactory().getCurrentSession();
        Customer car;
        session.beginTransaction();
        car = session.get(Customer.class, id);
        session.delete(car);
        session.getTransaction().commit();
    }

    public Customer getCustomer(int id){
        Session session = DBManager.getSessionFactory().getCurrentSession();
        Customer customer;
        session.beginTransaction();
        customer = session.get(Customer.class, id);
        session.getTransaction().commit();
        return customer;
    }
}
