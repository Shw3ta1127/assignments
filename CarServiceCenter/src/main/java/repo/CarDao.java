package repo;

import customers.Car;
import org.hibernate.Session;

public class CarDao {

    public void saveCar(Car car){
        Session session = DBManager.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(car);
        session.getTransaction().commit();
    }

    public void updateCar(Car car){
        Session session = DBManager.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.update(car);
        session.getTransaction().commit();
    }

    public void deleteCar(int id){
        Session session = DBManager.getSessionFactory().getCurrentSession();
        Car car;
        session.beginTransaction();
        car = session.get(Car.class, id);
        session.delete(car);
        session.getTransaction().commit();
    }

    public Car getCar(int id){
        Session session = DBManager.getSessionFactory().getCurrentSession();
        Car car;
        session.beginTransaction();
        car = session.get(Car.class, id);
        session.getTransaction().commit();
        return car;
    }
}
