package repo;

import managers.CarServiceStatus;
import org.hibernate.Session;

public class CarServiceStatusDao {

    public void saveStatus(CarServiceStatus status){
        Session session = DBManager.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(status);
        session.getTransaction().commit();
    }

    public void updateStatus(CarServiceStatus status){
        Session session = DBManager.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.update(status);
        session.getTransaction().commit();
    }

    public void deleteStatus(int id){
        Session session = DBManager.getSessionFactory().getCurrentSession();
        CarServiceStatus status;
        session.beginTransaction();
        status = session.get(CarServiceStatus.class, id);
        session.delete(status);
        session.getTransaction().commit();
    }

    public CarServiceStatus getStatus(int id){
        Session session = DBManager.getSessionFactory().getCurrentSession();
        CarServiceStatus status;
        session.beginTransaction();
        status = session.get(CarServiceStatus.class, id);
        session.getTransaction().commit();
        return status;
    }
}
