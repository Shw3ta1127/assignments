
import customers.*;
import exceptions.*;
import managers.CarServiceStatus;
import repo.*;
import managers.CarFuelManager;
import managers.CarServiceManager;
import managers.CarWashManager;

import java.util.Scanner;

public class HibernateMain {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Car car = new Car();
        Customer customer = new Customer();
        CarServiceStatus status = new CarServiceStatus();

        CarServiceManager carServiceManager = new CarServiceManager();
        CarFuelManager carFuelManager = new CarFuelManager();
        CarWashManager carWashManager = new CarWashManager();

        carWashManager.setWaterAvailable(true);
        carFuelManager.setAvailableFuel(100);

        CarDao carDao = new CarDao();
        CarServiceStatusDao carServiceStatusDao = new CarServiceStatusDao();
        CustomerDao customerDao = new CustomerDao();

        boolean isValidCustomer;

        while (true){
            System.out.print("Next Customer enter yes/no : ");
            if(scanner.next().equalsIgnoreCase("no"))
                break;

            userEntries(scanner, car, customer);

            isValidCustomer = false;

            try {
                isValidCustomer = carServiceManager.validateCustomer(customer);
            }
            catch (CapacityLessThanAvailableFuelException | NegativeValueException | CapacityZeroException e){
                System.out.println(e.getMessage());
            }

            if(! isValidCustomer){
                continue;
            }

            status.setFuelStatus(false);
            status.setWashStatus(false);
            status.setCar(car);

            carDao.saveCar(car);
            customerDao.saveCustomer(customer);
            carServiceStatusDao.saveStatus(status);

            System.out.println("\nCar Full Servicing Started...");
            try{
                status.setWashStatus(carWashManager.carWashing(car));
                status.setFuelStatus(carFuelManager.carFueling(car));
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
            System.out.println("Car Full Servicing Completed");
            carServiceManager.generateReport(status);
            carServiceStatusDao.updateStatus(status);
        }

    }

    private static void userEntries(Scanner scanner, Car car, Customer customer) {
        System.out.print("Enter Customer Name : ");
        customer.setName(scanner.next());

        System.out.print("Enter Customer Phone : ");
        customer.setPhone(scanner.next());

        System.out.print("Enter Car Type : ");
        car.setType(scanner.next());

        System.out.print("Enter Car Available Fuel : ");
        car.setAvailableFuel(scanner.nextInt());

        System.out.print("Enter Car Fuel Capacity : ");
        car.setCapacity(scanner.nextInt());

        customer.setCar(car);
    }
}
