package exceptions;

public class FuelNotAvailable extends RuntimeException{
    public String getMessage(){
        return "Fuel Not Available";
    }
}
