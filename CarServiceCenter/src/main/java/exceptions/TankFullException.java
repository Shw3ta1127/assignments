package exceptions;

public class TankFullException extends RuntimeException{
    public String getMessage(){
        return "Tank Full";
    }
}
