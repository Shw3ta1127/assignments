package exceptions;

public class WaterNotAvailable extends Exception{
    @Override
    public String getMessage(){
        return "Water not Available";
    }
}
