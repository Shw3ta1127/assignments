package exceptions;

public class CapacityLessThanAvailableFuelException extends RuntimeException{
    public String getMessage(){
        return "Capacity Less than Available fuel.";
    }
}
