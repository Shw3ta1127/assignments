package exceptions;

public class CapacityZeroException extends RuntimeException{
    public String getMessage(){
        return "Capacity Zero";
    }
}
